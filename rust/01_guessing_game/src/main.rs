use rand::Rng;
use std::cmp::Ordering;

fn guessing_game() {
    let secret_number = rand::thread_rng().gen_range(1..=100);
    println!("The secret number is: {secret_number}");

    loop {
        println!("Please input your guess.");
        let mut guess = String::new();
        std::io::stdin().read_line(&mut guess).expect("Failed to read line");

        let guess : u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };
        println!("You guessed: {guess}");

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small"),
            Ordering::Greater => println!("Too big"),
            Ordering::Equal => {
                println!("You win");
                break;
            }
        }
    }
}

fn tuple() {
    let tup = (500, 6.4, true);
    let (x,y,z) = tup;
    println!("tuple {:?} = {},{},{} = {},{},{}", tup, x, y, z, tup.0, tup.1, tup.2);
}

fn loop_control() {
    for number in 1..4 {
        println!("{number}!!");
    }
    println!("LIFTOFF!!!");

    for number in (1..4).rev() {
        println!("{number}!!");
    }
    println!("LIFTOFF!!!");
}

fn takes_ownership(some_string: String) {
    println!("{}", some_string);
}

fn takes_copy(some_integer: i32) {
    println!("{}", some_integer);
}

fn calc_len(s: String) -> (String, usize) {
    let length = s.len();
    (s, length)
}

fn test_ownership() {
    let s = String::from("Hello");
    takes_ownership(s);
    //println!("{}", s); // COMPILE ERROR: move occurs because `s` has type `String`, which does not implement the `Copy` trait

    let x = 5;
    takes_copy(x);
    println!("{}", x);

    let s1 = String::from("Hi");
    //drop(s1);
    let (s2, len) = calc_len(s1);
    println!("The length of '{}' is {}.", s2, len);
}

fn main() {
    guessing_game();
    tuple();
    loop_control();
    test_ownership();
}
