package com.example.study.a03_thread

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue

class Job(func : () -> Unit) {
    private var job: () -> Unit = func

    fun call() {
        job()
    }
}

class MainActivity : AppCompatActivity() {
    private var isJobRunning = false
    private var jobQ = LinkedBlockingQueue<Job>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        isJobRunning = true

        JobThread().start()
        Thread(Producer(jobQ)).start()
        Thread(Consumer(jobQ)).start()
        Thread(Consumer(jobQ)).start()
        Thread(Consumer(jobQ)).start()

        button1.setOnClickListener { _ ->
            var now1 = SystemClock.currentThreadTimeMillis()
            var now2 = System.currentTimeMillis()
            textView1.text = "time=${now1}, ${now2}"
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        isJobRunning = false
        jobQ.put(Job {})
        jobQ.put(Job {})
        jobQ.put(Job {})
    }

    inner class JobThread : Thread() {
        override fun run() {
            while (isJobRunning) {
                SystemClock.sleep(1000)
                var now1 = SystemClock.currentThreadTimeMillis()
                var now2 = System.currentTimeMillis()
                Log.d("abc", "time=${now1}, ${now2}")
            }
        }
    }

    inner class Producer(q : BlockingQueue<Job>) : Runnable {
        private var jobQ: BlockingQueue<Job> = q

        override fun run() {
            while (isJobRunning) {
                SystemClock.sleep(1000)
                var id = Thread.currentThread().id
                var now = System.currentTimeMillis()
                Log.d("def", "Producer: id=${id}, time=${now}..push")

                jobQ.put(Job {
                    var id = Thread.currentThread().id
                    Log.d("def", "job: id=${id}")
                })
            }
            var id = Thread.currentThread().id
            Log.d("def", "Producer: id=${id} END")
        }
    }

    inner class Consumer(q : BlockingQueue<Job>) : Runnable {
        private var jobQ: BlockingQueue<Job> = q

        override fun run() {
            while (isJobRunning) {
                var job = jobQ.take()
                var id = Thread.currentThread().id
                var now = System.currentTimeMillis()
                Log.d("def", "Consumer: id=${id}, time=${now}..pop")
                job.call()
            }
            var id = Thread.currentThread().id
            Log.d("def", "Consumer: id=${id} END")
        }
    }
}
