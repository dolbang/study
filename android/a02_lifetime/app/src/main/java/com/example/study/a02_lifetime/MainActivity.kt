package com.example.study.a02_lifetime

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.d("abcd", "onCreate")
    }

    override fun onStart() {
        super.onStart()
        Log.d("abcd", "onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.d("abcd", "onResume")
    }

    override fun onRestart() {
        super.onRestart()
        Log.d("abcd", "onRestart")
    }

    override fun onPause() {
        super.onPause()
        Log.d("abcd", "onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.d("abcd", "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("abcd", "onDestroy")
    }
}
