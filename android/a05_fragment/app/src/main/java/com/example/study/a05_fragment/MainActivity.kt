package com.example.study.a05_fragment

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var mFragmentA : FragmentA? = null
    private var mFragmentB : FragmentB? = null
    private var mFragmentC : FragmentC? = null
    private var mFragmentIndex = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mFragmentIndex = 0

        supportFragmentManager.beginTransaction()
                .replace(R.id.frame, getFragment(mFragmentIndex))
                .commit()

        button.setOnClickListener({_ ->
            ++mFragmentIndex
            mFragmentIndex %= 3
            supportFragmentManager.beginTransaction()
                    .replace(R.id.frame, getFragment(mFragmentIndex))
                    .commit()
        })
    }

    private fun getFragment(index : Int) : Fragment? {
        when (index) {
            0 -> {
                if (mFragmentA == null)
                    mFragmentA = FragmentA.newInstance("A1", "A2")
                return mFragmentA
            }
            1 -> {
                if (mFragmentB == null)
                    mFragmentB = FragmentB.newInstance("B1", "B2")
                return mFragmentB
            }
            2 -> {
                if (mFragmentC == null)
                    mFragmentC = FragmentC.newInstance("C1", "C2")
                return mFragmentC
            }
            else -> {
                return null
            }
        }
    }
}
