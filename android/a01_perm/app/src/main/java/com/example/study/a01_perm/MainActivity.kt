package com.example.study.a01_perm

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var permList = arrayOf(
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.READ_SMS
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        checkPermission()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        textView.text = ""

        for (idx : Int in grantResults.indices) {
            if (grantResults[idx] == PackageManager.PERMISSION_GRANTED)
                textView.append("${permList[idx]} = GRANTED\n")
            else
                textView.append("${permList[idx]} = DENIED\n")
        }
    }

    fun checkPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return
        for (perm : String in permList) {
            if (PackageManager.PERMISSION_DENIED == checkSelfPermission(perm)) {
                requestPermissions(permList, 0)
            }
        }
    }
}
