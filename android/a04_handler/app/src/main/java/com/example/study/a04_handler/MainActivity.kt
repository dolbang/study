package com.example.study.a04_handler

import android.os.*
import android.support.v7.app.AppCompatActivity
import android.util.Log

class MainActivity : AppCompatActivity() {
    private var isRunning = false
    private var mMainThreadId = Thread.currentThread().id
    private var mWorker1 : JobWorker? = null
    private var mWorker2 : JobWorker? = null
    private var mWorker3 : HandlerThread? = null
    private var mWorker4 : HandlerThread? = null
    private var mHandler3 : Handler? = null
    private var mHandler4 : Handler? = null
    private var mUIHandler : Handler? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        isRunning = true

        mWorker1 = JobWorker()
        mWorker1?.start()

        mWorker2 = JobWorker()
        mWorker2?.start()

        mWorker3 = HandlerThread("Worker3")
        mWorker3?.start()
        mHandler3 = Handler(mWorker3?.looper)

        mWorker4 = HandlerThread("Worker4")
        mWorker4?.start()
        mHandler4 = Handler(mWorker4?.looper)

        Thread(JobProducer()).start()

        mUIHandler = object : Handler() {
            override fun handleMessage(msg: Message?) {
                Log.d("abc", "UIHandler: id=${Thread.currentThread().id}, main=${mMainThreadId}")
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        isRunning = false
        mWorker1?.end()
        mWorker2?.end()
        mWorker3?.quitSafely()
        mWorker4?.quitSafely()
    }

    inner class JobProducer : Runnable {
        override fun run() {
            while (isRunning) {
                Log.d("abc", "JobProducer: id=${Thread.currentThread().id}, main=${mMainThreadId} +++++++++++++++++++++")
                mWorker1?.handler?.post({
                    Log.d("abc", "Worker1: id=${Thread.currentThread().id}, main=${mMainThreadId}")
                })
                mWorker2?.handler?.post({
                    Log.d("abc", "Worker2: id=${Thread.currentThread().id}, main=${mMainThreadId}")
                })
                mHandler3?.post({
                    Log.d("abc", "Worker3 id=${Thread.currentThread().id}, main=${mMainThreadId}")
                })
                mHandler4?.post({
                    Log.d("abc", "Worker4 id=${Thread.currentThread().id}, main=${mMainThreadId}")
                })
                mUIHandler?.sendEmptyMessage(0)
                mUIHandler?.post({
                    Log.d("abc", "UIHandler Runnable id=${Thread.currentThread().id}, main=${mMainThreadId}")
                })
                Log.d("abc", "JobProducer: id=${Thread.currentThread().id}, main=${mMainThreadId} ---------------------")
                SystemClock.sleep(1000)
            }
            Log.d("abc", "JobProducer end.")
        }
    }

    inner class JobWorker : Thread() {
        var handler : Handler? = null

        override fun run() {
            Looper.prepare()
            handler = Handler(Looper.myLooper())
            Looper.loop()
            Log.d("abc", "JobWorker end.")
        }

        fun end() {
            handler?.looper?.quitSafely()
        }
    }
}
