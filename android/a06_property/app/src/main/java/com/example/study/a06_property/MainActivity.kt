package com.example.study.a06_property

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var nullable: MyMath? = null
    lateinit var nonNull: MyMath
    val readOnly: MyMath by lazy {
        MyMath()
    }
    @Volatile var count: Int = 0
    @Volatile private var isRunning = false
    private var lock: Any = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        nullable?.mul(2, 2)
//        nonNull.mul(2, 2)        // compile: OK, runtime: NPE

        nullable = MyMath()
        nonNull = MyMath()

        button.setOnClickListener { _ ->
            Log.d("abc", "nullable?.add = ${nullable?.add(10, 2) ?: 0}")
            Log.d("abc", "nonNull.sub   = ${nonNull.sub(10, 2)}")
            Log.d("abc", "readOnly.div  = ${readOnly.div(10, 2)}")
            Log.d("abc", "readOnly.mul  = ${readOnly.mul(10, 2)}")
        }

        isRunning = true

        object: Thread() {
            override fun run() {
                while (isRunning) {
                    var v = synchronized(lock, {
                        ++count
                    })
                }
            }
        }.start()

        Thread(object: Runnable {
            override fun run() {
                while (isRunning) {
                    var v = synchronized(lock, {
                        --count
                    })
                }
            }
        }).start()

        object: Thread() {
            override fun run() {
                while (isRunning) {
                    SystemClock.sleep(200)
                    var st = System.currentTimeMillis()
                    var v = count
                    var du = System.currentTimeMillis() - st
                    Log.d("abc", "count= ${v} du=${du}")
                }
            }
        }.start()
    }

    override fun onDestroy() {
        super.onDestroy()
        isRunning = false
    }

    inner class MyMath {
        init {
            Log.d("abc", "MyMath.init")
        }
        fun add(a: Int, b: Int) = a + b
        fun sub(a: Int, b: Int) = a - b
        fun div(a: Int, b: Int) = a / b
        fun mul(a: Int, b: Int) = a * b
    }
}
