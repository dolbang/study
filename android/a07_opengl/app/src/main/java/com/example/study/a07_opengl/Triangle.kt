package com.example.study.a07_opengl

import android.opengl.GLES30
import android.util.Log
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer

class Triangle {
    val COORDS_PER_VERTEX = 3
    val BYTES_PER_COORDS = 4

    private val mCoords = arrayOf(
            0.0f, 0.6f, 0.0f,
            -0.5f, -0.3f, 0.0f,
            0.5f, -0.3f, 0.0f)

    private val mColor = arrayOf(1.0f, 0.0f, 0.0f, 1.0f)

    private val mVertexBuffer: FloatBuffer by lazy {
        var bb = ByteBuffer.allocateDirect(mCoords.size * BYTES_PER_COORDS)
        with(bb) {
            order(ByteOrder.nativeOrder())
            asFloatBuffer()
        }
    }

    private val mVertexShader: Int by lazy {
        val code =
            "attribute vec4 vPosition;" +
            "void main() {" +
            "   gl_Position = vPosition;" +
            "}"

        GLUT.loadShader(GLES30.GL_VERTEX_SHADER, code)
    }

    private val mFragmentShader: Int by lazy {
        val code =
            "precision mediump float;" +
            "uniform vec4 vColor;" +
            "void main() {" +
            "   gl_FragColor = vColor;" +
            "}"

        GLUT.loadShader(GLES30.GL_FRAGMENT_SHADER, code)
    }

    private val mProgram: Int by lazy {
        GLES30.glCreateProgram()
    }

    init {
        with(mVertexBuffer) {
            put(mCoords.toFloatArray())
            position(0)
        }

        GLES30.glAttachShader(mProgram, mVertexShader)
        GLES30.glAttachShader(mProgram, mFragmentShader)
        GLES30.glLinkProgram(mProgram)
    }

    fun draw() {
        Log.d("abc", "Triangle.draw")
        GLES30.glUseProgram(mProgram)

        var positionHandle = GLES30.glGetAttribLocation(mProgram, "vPosition")
        GLES30.glEnableVertexAttribArray(positionHandle)
        GLES30.glVertexAttribPointer(positionHandle, COORDS_PER_VERTEX, GLES30.GL_FLOAT, false, COORDS_PER_VERTEX * BYTES_PER_COORDS, mVertexBuffer)

        var colorHandle = GLES30.glGetUniformLocation(mProgram, "vColor")
        GLES30.glUniform4fv(colorHandle, 1, mColor.toFloatArray(), 0)

        GLES30.glDrawArrays(GLES30.GL_TRIANGLES, 0, mCoords.size / COORDS_PER_VERTEX)

        GLES30.glDisableVertexAttribArray(positionHandle)
    }
}