package com.example.study.a07_opengl

import android.content.Context
import android.opengl.GLSurfaceView
import android.util.Log

class MyGLSurfaceView(context: Context) : GLSurfaceView(context) {
    private val mRenderer: MyGLRenderer by lazy {
        Log.d("abc", "MyGLRenderer object created")
        MyGLRenderer()
    }

    init {
        Log.d("abc", "MyGLSurfaceView.init")
        setEGLContextClientVersion(3)

        setRenderer(mRenderer)
        renderMode = GLSurfaceView.RENDERMODE_WHEN_DIRTY
    }
}
