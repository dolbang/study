package com.example.study.a07_opengl

import android.opengl.GLES30

object GLUT {
    fun loadShader(type: Int, code: String) : Int {
        var shader = GLES30.glCreateShader(type)
        GLES30.glShaderSource(shader, code)
        GLES30.glCompileShader(shader)
        return shader
    }
}