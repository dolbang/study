package com.example.study.a07_opengl

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity() {
    private val mGLView: MyGLSurfaceView by lazy {
        Log.d("abc", "MyGLSurfaceView object created")
        MyGLSurfaceView(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("abc", "MainActivity.onCreate")
        setContentView(mGLView)
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("abc", "MainActivity.onDestroy")
    }
}
