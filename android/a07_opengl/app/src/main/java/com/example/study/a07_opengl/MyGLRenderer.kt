package com.example.study.a07_opengl

import android.opengl.GLES30
import android.opengl.GLSurfaceView
import android.util.Log
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

class MyGLRenderer : GLSurfaceView.Renderer {
    private val mTriangle: Triangle by lazy {
        Triangle()
    }

    override fun onSurfaceCreated(gl: GL10?, config: EGLConfig?) {
        Log.d("abc", "MyGLRenderer.onSurfaceCreated")
        GLES30.glClearColor(0.0f, 0.0f, 0.0f, 1.0f)
    }

    override fun onSurfaceChanged(gl: GL10?, width: Int, height: Int) {
        Log.d("abc", "MyGLRenderer.onSurfaceChanged")
        GLES30.glViewport(0, 0, width, height)
    }

    override fun onDrawFrame(gl: GL10?) {
        GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT)
        mTriangle.draw()
    }
}